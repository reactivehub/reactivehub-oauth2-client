authModule.constant("authResources", {
  Validation:
    Errors:
      RedirectURINotSpecified: "Redirect URI not specified!"
      ClientIDNotSpecified: "Client ID not specified!"
      AuthorizeEndpointNotSpecified: "Login URL not specified!"
      ApiURLMatcherNotSpecified: "API URL matcher (prefix or function) not specified!"
    Messages:
      AllRequiredParamsPresent: "All required params present."
  Config:
    Defaults:
      TokenInfoCookieName: "apiAuthTokenInfo"
      StateCookieName: "apiAuthState"
      SessionCookieNames: ["session"]
      RevocationEndpoint: "/revoke"
})
