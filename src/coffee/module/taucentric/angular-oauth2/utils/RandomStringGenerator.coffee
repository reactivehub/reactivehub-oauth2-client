class RandomStringService

  generate: () ->
    if @hasCryptoSupport()
      @generateCryptoRandom()
    else
      @generateMathRandom()

  hasCryptoSupport: () ->
    _.isFunction(window.crypto.getRandomValues)

  generateCryptoRandom: (size = 8) ->
    _.reduce(@randomlyFillBuffer(8),(prev, randomNumber) ->
      "#{prev}#{randomNumber.toString(36)}"
    ,"")

  randomlyFillBuffer: (size) ->
    buf = new Uint8Array(size)
    window.crypto.getRandomValues(buf)
    buf

  generateMathRandom: (size = 8) ->
    sizePowered = Math.pow(10, size)
    _.reduce([0..(size-1)],(prev, num) =>
      "#{prev}#{@generatePseudoRandomNumber().toString(36)}"
    ,"")

  generatePseudoRandomNumber: (max = 36) -> Math.round(Math.random() * max)


authModule.factory(ModuleNames.Services.RandomStringService, ["$log", ($log) ->
  new RandomStringService($log)
])