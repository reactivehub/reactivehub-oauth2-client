if not _.isFunction(String::startsWith)
  String::startsWith = (str) ->
    if str
      @slice(0, str.length) == str
    else
      false