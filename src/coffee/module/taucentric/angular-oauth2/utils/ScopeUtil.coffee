class ScopeUtil

  @safeApply: (scope) ->
    (fn) ->
      phase = scope.$root.$$phase

      if phase == "$apply" or phase == "$digest"
        if _.isFunction(fn)
          fn()
      else
        scope.$apply(fn)