class ApiURLMatcher

  @matches: (url, apiURLMatcher) ->
    if _.isFunction(apiURLMatcher)
      apiURLMatcher(url)
    else
      url and url.startsWith(apiURLMatcher)