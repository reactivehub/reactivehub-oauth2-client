class QueryParser
  
  @parse: (path, params) ->
    _.pick(_.reduce(_.map(path.split("&"),(kv) ->
      kvList = kv.split("=")
    ),(res, tuple) ->
      _.extend(res, _.object([tuple[0]], [tuple[1]]))
    ,{}), params)