authModule.directive("logoutButton", ["$compile", "$log", "authService", ($compile, $log, authService) ->
  {
    restrict: "A"

    link: (scope, element, attrs) ->

      scope.logoutAction = () ->
        authService.logout()

      element.on("click", (evt) ->
        ScopeUtil.safeApply(scope)(() ->
          scope.logoutAction()
        )
      )

  }
])
