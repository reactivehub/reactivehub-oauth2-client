#_require TokenExtractor

class AccessTokenResponseExtractor extends TokenExtractor
  
  hasValidResponseState: (accessTokenResponse) ->
    accessTokenResponse?.state and accessTokenResponse.state == @getTokenStorageService().getState()

  extract: (prevError) ->
    result = null
    path = @path.substring(1)
    accessTokenResponse = @parseAccessTokenResponse(path)

    if not @hasValidResponseState(accessTokenResponse)
      result = @q.reject("invalid_state")
    else if not @validateAccessTokenResponse(accessTokenResponse)
      result = @q.reject("invalid_response")
    else
      result = @q.when(@extractTokenInfoFromResponse(accessTokenResponse))

    @getTokenStorageService().removeState()

    result

  extractTokenInfoFromResponse: (response) ->
    {
      token: response.access_token
      expirationTime: @getExpirationTime(response.expires_in)
    }

  validateAccessTokenResponse: (accessTokenResponse) ->
    if accessTokenResponse?.access_token
      true
    else
      false

  getExpirationTime: (expiresIn) ->
    if expiresIn
      @getNow() + expiresIn * 1000
    else
      null

  getNow: () -> Date.now()

  parseAccessTokenResponse: (path) -> AccessTokenResponse.parse(path)

