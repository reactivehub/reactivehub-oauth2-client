#_require TokenExtractor

class AccessTokenErrorExtractor extends TokenExtractor

  extract: (prevError) ->
    result = null
    path = @path.substring(1)
    accessTokenError = @parseAccessTokenError(path)

    if accessTokenError.error
      @q.reject(accessTokenError)
    else
      @q.reject(prevError)

  parseAccessTokenError: (path) -> AccessTokenError.parse(path)

