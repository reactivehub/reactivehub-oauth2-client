#_require TokenExtractor

class CookieExtractor extends TokenExtractor
  
  extract: () ->
    tokenStorage = @getTokenStorageService()

    tokenInfo = tokenStorage.getTokenInfo()
    if tokenInfo
      @q.when(tokenInfo)
    else
      @q.reject("Token not found in token storage...")
