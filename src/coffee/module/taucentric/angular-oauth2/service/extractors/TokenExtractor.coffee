class TokenExtractor
  
  constructor: (@path, @$injector) ->
    @log = @getLog()
    @q = @$injector.get("$q")

  extract: () -> @q.reject("Not implemented!")

  getTokenStorageService: () -> @$injector.get("tokenStorageService")

  getLog: () -> @$injector.get("$log")