class HttpRequestInterceptor

  @tokenType = "Bearer"

  # We need to include token in all requests to API or request to revocationEndpoint
  @intercept: (config, authService) ->
    result = config
    if ApiURLMatcher.matches(config?.url, authService?.config?.apiURLMatcher) or ApiURLMatcher.matches(config?.url, authService?.config?.revocationEndpoint)

      result = authService.getToken().then((token) ->
        oldHeaders = config.headers or {}
        newHeaders = _.extend(oldHeaders, {
          Authorization: "#{HttpRequestInterceptor.tokenType} #{token}"
        })
        _.extend(config, {headers: newHeaders})
      )

    result

class HttpResponseInterceptor

  @intercept: ($q, rejection, authService) ->
    if ApiURLMatcher.matches(rejection.config?.url, authService?.config?.apiURLMatcher) and authService.config.loginRedirectionFilter(rejection.status)
      authService.revokeTokenInfo()
    $q.reject(rejection)


authModule.config(["$httpProvider", ($httpProvider) ->
  $httpProvider.interceptors.push(($q, authService) ->
    request: (config) -> HttpRequestInterceptor.intercept(config, authService)
    responseError: (rejection) -> HttpResponseInterceptor.intercept($q, rejection, authService)
  )
])
