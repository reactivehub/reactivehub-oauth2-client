class TokenStorageService

  constructor: (@authService, @$cookieStore, @$rootScope, @$log) ->
    #@$log.log("TokenStorageService created ...")

  getConfigValue: (parameterName) -> @authService.config?[parameterName]
  getTokenCookieName: () -> @getConfigValue("tokenInfoCookieName")
  getStateCookieName: () -> @getConfigValue("stateCookieName")
  getSessionCookieNames: () -> @getConfigValue("sessionCookieNames")

  getTokenInfo: () -> @get(@getTokenCookieName())

  storeTokenInfo: (tokenInfo) ->
    @store(@getTokenCookieName(), tokenInfo)

  revokeTokenInfo: () ->
    @remove(@getTokenCookieName())
    for sessionCookie in (@getSessionCookieNames() or [])
      @remove(sessionCookie)

  getState: () -> @get(@getStateCookieName())

  storeState: (state) -> @store(@getStateCookieName(), state)

  removeState: () -> @remove(@getStateCookieName())


  get: (name) ->
    @$cookieStore.get(name)

  store: (name, value) ->
    ScopeUtil.safeApply(@$rootScope)(() =>
      @$cookieStore.put(name, value)
    )


  remove: (name) ->
    ScopeUtil.safeApply(@$rootScope)(() =>
      @$cookieStore.remove(name)
    )


authModule.factory(ModuleNames.Services.TokenStorageService, [ModuleNames.Services.AuthService, "$cookieStore", "$rootScope", "$log", (authService, $cookieStore, $rootScope, $log) ->
  new TokenStorageService(authService, $cookieStore, $rootScope, $log)
])
