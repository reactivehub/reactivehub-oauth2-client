class AuthorizationRequestService

  @defaultTokenType = "token"

  constructor: (@tokenStorageService, @randomStringService, @log, @q) ->

  # Generates random string to be used as state
  # parameter in access token request.
  generateState: () -> @randomStringService.generate()

  # Generates and stores state in token storage.
  # State will be matched with incoming state in query when browser
  # is redirected back from authorization server.
  generateAndStoreState: () ->
    state = @generateState()
    @tokenStorageService.storeState(state)
    state

  # Generates new authorization request object.
  #
  # - state is generated and stored as part of this method
  #
  generateRequest: (clientID, redirectURI, scope = null) ->
    state = @generateAndStoreState()
    new AuthorizationRequest(AuthorizationRequestService.defaultTokenType, clientID, redirectURI, scope, state, @q)
     
authModule.factory(ModuleNames.Services.AuthorizationRequestService, [
  ModuleNames.Services.TokenStorageService
  ModuleNames.Services.RandomStringService
  "$log"
  "$q"
  (tokenStorageService, randomStringService, $log, $q) ->
    new AuthorizationRequestService(tokenStorageService, randomStringService, $log, $q)
])