#_require extractors/AccessTokenResponseExtractor
#_require extractors/CookieExtractor
#_require extractors/AccessTokenErrorExtractor

class AuthService

  constructor: (@config, @$injector) ->

    @tokenRequests = []
    @tokenInitializationComplete = false

    @log = @getLogService()
    @originalPath = @getLocationService().path()
    @q = @$injector.get("$q")

    @initializeTokenInfo().then((tokenInfo) =>
      @tokenInitializationComplete = true
      @storeTokenInfo(tokenInfo)
      @resolveTokenRequests()
    , (error) =>
      @tokenInitializationComplete = true
      @revokeTokenInfo()
      @rejectTokenRequests(error)
    )

  getLocationService: () ->             @$injector.get("$location")
  getLogService: () ->                  @$injector.get("$log")
  getTokenStorageService: () ->         @$injector.get(ModuleNames.Services.TokenStorageService)
  getAuthorizationRequestService: () -> @$injector.get(ModuleNames.Services.AuthorizationRequestService)
  getExtractorClasses: () ->            @$injector.get(ModuleNames.Constants.Extractors)
  getHttpService: () ->                 @$injector.get("$http")

  # Method tries to extract token from different places by executing
  # list of specified TokenExtractors taking the first result returning
  # token value.
  #
  # AccessTokenResponseExtractor - tries to extract token from URL fragment.
  # URL fragment contains token when redirected back from authorization
  # server.
  #
  # CookieExtractor - tries to retrieve the token from cookie

  initializeTokenInfo: () ->

    reduceHandler = (extractorClass) => (prevError) => @extractWithExtractor(extractorClass, prevError)

    _.reduce(@getExtractorClasses(),(prev, extractorClass) =>
      prev.then(null, reduceHandler(extractorClass))
    , @q.reject("not_found"))


  extractWithExtractor: (extractorClass, prevError) =>
    extractor = new extractorClass(@originalPath, @$injector)
    extractor.extract(prevError)

  getToken: () ->
    result = @q.defer()
    if @tokenInitializationComplete
      @resolveFutureWithToken(result)
    else
      @addToTokenRequests(result)

    promise = result.promise

    promise.then((token) ->
      token
    , (rejection) =>
      @goToAuthorizationServer()
    )

    promise


  validateExpirationTime: (tokenInfo) ->
    if tokenInfo.expirationTime
      tokenInfo.expirationTime > @getNowPlus(10)
    else
      true

  getNowPlus: (minutes = 0) -> Date.now() + minutes * 60 * 1000

  addToTokenRequests: (future) ->
    @tokenRequests.push(future)


  # Resolves awaiting token requests with token value.
  # If token not found, token requests are rejected with
  # 'not_found' error. If token is expired, token requests
  # are rejected with 'expired' error.
  resolveTokenRequests: () ->
    for tokenRequest in @tokenRequests
      @resolveFutureWithToken(tokenRequest)

    @tokenRequests = []

  rejectTokenRequests: (error) ->
    for tokenRequest in @tokenRequests
      @rejectFutureWithError(tokenRequest, error)

    @tokenRequests = []


  resolveFutureWithToken: (future) ->
    if @tokenInfo and @tokenInfo.token
      if @validateExpirationTime(@tokenInfo)
        future.resolve(@tokenInfo.token)
      else
        future.reject("expired")
    else
      future.reject("not_found")

  rejectFutureWithError: (future, error) ->
    future.reject(error)


  storeTokenInfo: (@tokenInfo) ->
    @getTokenStorageService().storeTokenInfo(@tokenInfo)

  # Method clears currently used token:
  # - clears locally kept token
  # - clears token stored in cookie
  # - redirects back to login page
  revokeTokenInfo: () ->
    @tokenInfo = null
    @getTokenStorageService().revokeTokenInfo()

  # Asks AuthorizationRequestService for fresh authorization request object.
  generateAuthorizationRequest: () ->
    @getAuthorizationRequestService().generateRequest(@config.clientID, @config.redirectURI, @config.scope)

  # Method redirects the browser to authorization endpoint
  # - it firstly generates authorization request
  # - then the request is validated (whether it contains all required params)
  # - if valid, browser is redirected to the authorization endpoint (else error is logged)
  goToAuthorizationServer: () ->
    authorizationRequest = @generateAuthorizationRequest()

    authorizationRequest.validate().then(() =>
      authorizeEndpointURL = "#{@config.authorizeEndpoint}?#{authorizationRequest.getQueryParams()}"

      #@log.log("Authorize Endpoint: #{authorizeEndpointURL}")
      window.location.replace(authorizeEndpointURL)
    ,(error) =>
      @log.error(error)
      error
    )

  logout: () ->
    @getHttpService().post(@config.revocationEndpoint).then((result) =>
      @revokeTokenInfo()
      @goToAuthorizationServer()
    , (error) =>
      @log.error(error)
    )



  enrichURIWithAccessToken: (uri) ->
    uri = if uri then uri else ""
    @getToken().then((token) =>
      "#{uri}?access_token=#{token}"
    )



# AuthServiceProvider is AuthService factory and is used
# to configure AuthService service.
#
# To configure AuthService, inject authServiceProvider in AngularJS
# config block and provide the configuration object by calling
# authServiceProvider.config(configObj) where configObj contains:
#
# clientID - (mandatory) ID of the client registered with Authorization Server
#
# redirectURI - (mandatory) redirect URI where the user-agent should be redirected when
# coming back from Authorization Server
#
# authorizeEndpoint - (mandatory) Authorization server endpoint where Access Token Request
# should be send.
#
# revocationEndpoint - (optional) Revocation endpoint where the request should be POSTed
# to revoke token. Default is /revoke
#
# apiURLMatcher - (mandatory) API URL matcher (either prefix string or string -> boolean function) -
# all calls to endpoints starting with specified api URL prefix (or provided function returns true for URL being called)
# will be intercepted and token will be included as Authorize HTTP request header.
#
# scope - (optional) Space-delimited list of required scopes
#
# loginRedirectionFilter - (optional) Function int -> boolean which determines
# whether HTTP response with specified error code should be redirected to
# login page.
#
# tokenInfoCookieName - (optional) Cookie name for storing token value. Default is apiAuthTokenInfo.
#
# stateCookieName - (optional) Cookie name for storing state being send to authorization server
# in access token request


class AuthServiceProvider

  constructor: (@authResources) ->

  getDefaultConfigData: () ->
    loginRedirectionFilter: (code) -> code == 401
    tokenInfoCookieName: @authResources.Config.Defaults.TokenInfoCookieName
    stateCookieName: @authResources.Config.Defaults.StateCookieName
    sessionCookieNames: @authResources.Config.Defaults.SessionCookieNames
    revocationEndpoint: @authResources.Config.Defaults.RevocationEndpoint

  extendDefaultConfig: (configData) ->
    picked = _.pick(configData, AuthServiceConfigValidator.parameters)
    _.extend(@getDefaultConfigData(), picked)

  config: (configData) ->
    @configData = @extendDefaultConfig(configData)
    @validate(@configData)

  validate: (config) ->
    new AuthServiceConfigValidator(config, @authResources).validate()

  $get: ($injector) ->
    new AuthService(@configData, $injector)


# AuthService extractor classes
authModule.constant(ModuleNames.Constants.Extractors, [
  AccessTokenResponseExtractor
  CookieExtractor
  AccessTokenErrorExtractor
])

# AuthServiceProvider registration
authModule.provider(ModuleNames.Services.AuthService, AuthServiceProvider)

# Explicitly inject authService to initialize the service.
authModule.run([ModuleNames.Services.AuthService, (authService) -> ])
