class AccessTokenResponse
  
  @accessTokenParam = "access_token"
  @tokenTypeParam = "token_type"
  @expiresInParam = "expires_in"
  @scopeParam = "scope"
  @stateParam = "state"

  @params = [
    @accessTokenParam
    @tokenTypeParam
    @expiresInParam
    @scopeParam
    @stateParam
  ]

  @parse: (path) -> QueryParser.parse(path, @params)