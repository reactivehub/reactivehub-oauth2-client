class AccessTokenError
  
  @errorParam = "error"
  @errorDescriptionParam = "error_description"
  @errorUriParam = "error_uri"

  @params = [
    @errorParam
    @errorDescriptionParam
    @errorUriParam
  ]

  @parse: (path) -> QueryParser.parse(path, @params)
    