class AuthorizationRequest

  @responseTypeParam = "response_type"
  @clientIDParam = "client_id"
  @redirectURIParam = "redirect_uri"
  @scopeParam = "scope"
  @stateParam = "state"
  
  constructor: (@responseType, @clientID, @redirectURI, @scope, @state, @q) ->
    @params = [
      {
        key: AuthorizationRequest.responseTypeParam
        value: @responseType
      }
      {
        key: AuthorizationRequest.clientIDParam
        value: @clientID
      }
      {
        key: AuthorizationRequest.redirectURIParam
        value: @redirectURI
      }
      {
        key: AuthorizationRequest.scopeParam
        value: @scope
      }
      {
        key: AuthorizationRequest.stateParam
        value: @state
      }
    ]

  validate: () ->
    result = @responseType and @clientID and @state
    if result
      @q.when(@)
    else
      @q.reject("Authorization Request does not contain all mandatory fields (response_type, client_id, state)!")

  getQueryParams: () ->

    toKV = (p) -> "#{p.key}=#{encodeURIComponent(p.value)}"

    _.reduce(@params, (prev, param) ->
      result = prev
      if param.value
        if prev
          result = "#{prev}&#{toKV(param)}"
        else
          result = toKV(param)
      result
    ,"")

