#_require ConfigValidator

class AuthServiceConfigValidator extends ConfigValidator

  @parameters = [
    "clientID"
    "redirectURI"
    "authorizeEndpoint"
    "apiURLMatcher"
    "loginRedirectionFilter"
    "tokenInfoCookieName"
    "stateCookieName"
    "sessionCookieNames"
  ]

  runValidation: () ->
    @validateRequiredParams({
      clientID: @resources.Validation.Errors.ClientIDNotSpecified
      redirectURI: @resources.Validation.Errors.RedirectURINotSpecified
      authorizeEndpoint: @resources.Validation.Errors.AuthorizeEndpointNotSpecified
      apiURLMatcher: @resources.Validation.Errors.ApiURLMatcherNotSpecified
    })
