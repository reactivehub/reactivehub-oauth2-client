class ConfigValidator

  constructor: (@config, @resources) ->

  runValidation: () ->

  validate: () -> @runValidation()

  validateRequiredParams: (params) ->
    
    for parameterName, errorMessage of params
      if not @config or not @config[parameterName]
        return Q.reject(errorMessage)

    Q(@resources.Validation.Messages.AllRequiredParamsPresent)
