ModuleNames =
  ModuleName: "taucentric.client.oauth2"
  Services:
    AuthService: "authService"
    TokenStorageService: "tokenStorageService"
    AuthorizationRequestService: "authorizationRequestService"
    RandomStringService: "randomStringService"
  Constants:
    Extractors: "authService.extractors"

authModule = angular.module(ModuleNames.ModuleName, ["ngCookies"])
