assertAsync = (guardFunc, assertionsFunc) ->
  waitsFor(guardFunc, "promise to be resolved/rejected", 100)
  runs(assertionsFunc)