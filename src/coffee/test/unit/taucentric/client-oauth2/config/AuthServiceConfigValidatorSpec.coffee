describe("AuthServiceConfigValidator", () ->

  resources = null

  beforeEach(() ->
    module(ModuleNames.ModuleName)
    inject((authResources) ->
      resources = authResources
    )
  )

  it("should validate auth service config", () ->

    config = {
      clientID: "foo"
      redirectURI: "http://api.bar.com"
      authorizeEndpoint: "http://as.bar.com"
      apiURLMatcher: (url) -> true
    }

    validator = new AuthServiceConfigValidator(config, resources)
    promise = validator.validate()

    assertAsync(() ->
      promise.isFulfilled()
    ,() ->

    )
  )

  it("should check if clientID is present", () ->

    config = {
      redirectURI: "http://api.bar.com"
      authorizeEndpoint: "http://as.bar.com"
      apiURLMatcher: (url) -> true
    }

    validator = new AuthServiceConfigValidator(config, resources)
    promise = validator.validate()

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      value = promise.inspect().reason
      expect(value).toEqual(resources.Validation.Errors.ClientIDNotSpecified)
    )
  )

  it("should check if redirect URI is present", () ->

    config = {
      clientID: "123"
      authorizeEndpoint: "http://as.bar.com"
      apiURLMatcher: (url) -> true
    }

    validator = new AuthServiceConfigValidator(config, resources)
    promise = validator.validate()

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      value = promise.inspect().reason
      expect(value).toEqual(resources.Validation.Errors.RedirectURINotSpecified)
    )
  )

  it("should check if authorize endpoint is present", () ->

    config = {
      clientID: "123"
      redirectURI: "http://api.bar.com"
      apiURLMatcher: (url) -> true
    }

    validator = new AuthServiceConfigValidator(config, resources)
    promise = validator.validate()

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      value = promise.inspect().reason
      expect(value).toEqual(resources.Validation.Errors.AuthorizeEndpointNotSpecified)
    )
  )

  it("should check if api URL is present", () ->

    config = {
      clientID: "123"
      redirectURI: "http://api.bar.com"
      authorizeEndpoint: "foo"
    }

    validator = new AuthServiceConfigValidator(config, resources)
    promise = validator.validate()

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      value = promise.inspect().reason
      expect(value).toEqual(resources.Validation.Errors.ApiURLMatcherNotSpecified)
    )
  )


)