describe("RandomStringGenerator", () ->

  service = null

  beforeEach(() ->
    module("taucentric.client.oauth2")
    inject((randomStringService) ->
      service = randomStringService
    )
  )


  it("should provide generate method",() ->
    expect(service.generate).toBeDefined()
  )

  it("should use generateMathRandom when window.crypto not defined",() ->

    generateMathRandom = spyOn(service, "generateMathRandom")
    generateCryptoRandom = spyOn(service, "generateCryptoRandom")
    
    service.hasCryptoSupport = () -> false
    service.generate()
    
    expect(generateMathRandom).toHaveBeenCalled()
    expect(generateCryptoRandom).not.toHaveBeenCalled()
  )

  it("should use generateMathRandom when window.crypto not defined",() ->

    generateMathRandom = spyOn(service, "generateMathRandom")
    generateCryptoRandom = spyOn(service, "generateCryptoRandom")
    
    service.hasCryptoSupport = () -> true
    service.generate()

    expect(generateMathRandom).not.toHaveBeenCalled()
    expect(generateCryptoRandom).toHaveBeenCalled()
  )

  it("should generate correct random number with window.crypto",() ->

    service.randomlyFillBuffer = () -> [0..35]
    result = service.generateCryptoRandom()

    expect(result).toBe("0123456789abcdefghijklmnopqrstuvwxyz")
  )

  it("should generate correct random number without window.crypto",() ->

    service.generatePseudoRandomNumber = () -> 10
    result = service.generateMathRandom(8)

    expect(result).toBe("aaaaaaaa")

    service.generatePseudoRandomNumber = () -> 1
    result = service.generateMathRandom(4)

    expect(result).toBe("1111")
  )

  

)