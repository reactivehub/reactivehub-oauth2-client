describe("ApiURLMatcher", () ->

  beforeEach(() ->
    module("taucentric.client.oauth2")
  )

  it("should check the prefix if apiURLMatcher is a string", () ->
    expect(ApiURLMatcher.matches("xyz", "xy")).toBe(true)
  )

  it("should execute apiURLMatcher if a function", () ->
    matcher = jasmine.createSpy("apiURLMatcher")
    ApiURLMatcher.matches("xyz", matcher)
    expect(matcher).toHaveBeenCalledWith("xyz")
  )

  it("should not match when apiURLMatcher returns false", () ->
    matcher = jasmine.createSpy("apiURLMatcher").andCallFake(() -> false)
    res = ApiURLMatcher.matches("xyz", matcher)
    expect(res).toBe(false)
  )

  it("should match when apiURLMatcher returns true", () ->
    matcher = jasmine.createSpy("apiURLMatcher").andCallFake(() -> true)
    res = ApiURLMatcher.matches("xyz", matcher)
    expect(res).toBe(true)
  )

  it("should not match when destination URL is not defined", () ->
    expect(ApiURLMatcher.matches(null, "xy")).toBeFalsy()
    expect(ApiURLMatcher.matches(undefined, "xy")).toBeFalsy()
    expect(ApiURLMatcher.matches("", "xy")).toBeFalsy()
  )

  it("should not match when apiURLMatcher is not defined", () ->
    expect(ApiURLMatcher.matches("xyz", null)).toBeFalsy()
    expect(ApiURLMatcher.matches("xyz", undefined)).toBeFalsy()
    expect(ApiURLMatcher.matches("xyz", "")).toBeFalsy()
  )

  it("should not match when apiURLMatcher is not defined and destination URL is not defined", () ->
    expect(ApiURLMatcher.matches(null, null)).toBeFalsy()
    expect(ApiURLMatcher.matches(undefined, undefined)).toBeFalsy()
    expect(ApiURLMatcher.matches("", "")).toBeFalsy()
  )
  

)