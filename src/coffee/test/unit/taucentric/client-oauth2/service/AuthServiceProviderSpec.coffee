describe("AuthServiceProvider", () ->

  resources = null

  beforeEach(() ->
    module("taucentric.client.oauth2")
    inject((authResources) ->
      resources = authResources
    )
  )

  it("should provide default loginRedirectionFilter function", () ->
    provider = new AuthServiceProvider(resources)
    defaultConfig = provider.getDefaultConfigData()
    
    expect(_.isFunction(defaultConfig.loginRedirectionFilter)).toBe(true)
  )

  it("should provide default name for tokenInfo cookie", () ->
    provider = new AuthServiceProvider(resources)
    defaultConfig = provider.getDefaultConfigData()
    
    expect(defaultConfig.tokenInfoCookieName).toEqual(resources.Config.Defaults.TokenInfoCookieName)
    
  )

  it("should provide default name for state cookie", () ->
    provider = new AuthServiceProvider(resources)
    defaultConfig = provider.getDefaultConfigData()
    
    expect(defaultConfig.stateCookieName).toEqual(resources.Config.Defaults.StateCookieName)
    
  )

  it("should override default config params with provided values", () ->
    provider = new AuthServiceProvider(resources)
    
    config =
      clientID: "abc"
      authorizeEndpoint: "http://foo.com"
      apiURLMatcher: (s) -> true
      tokenInfoCookieName: "abc"
      stateCookieName: "def"
      xyz: "xyz"

    result = provider.extendDefaultConfig(config)

    expect(result.clientID).toEqual("abc")
    expect(result.authorizeEndpoint).toEqual("http://foo.com")
    expect(_.isFunction(result.apiURLMatcher)).toBe(true)
    expect(result.tokenInfoCookieName).toEqual("abc")
    expect(result.stateCookieName).toEqual("def")
    expect(result.xyz).toBeUndefined()

  )

  it("should always validate the config when config is being added", () ->
    provider = new AuthServiceProvider(resources)
    defaultConfig = provider.getDefaultConfigData()
    validateSpy = spyOn(provider, "validate")
    
    provider.config(defaultConfig)
    expect(validateSpy).toHaveBeenCalledWith(defaultConfig)
  )

)