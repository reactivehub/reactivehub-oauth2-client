describe("AccessTokenError", () ->

  beforeEach(() ->
    module("taucentric.client.oauth2")
  )

  it("should parse access token error from the URL fragment", () ->
    path = "error=xyz&error_description=desc&error_uri=uri"

    expectedResult = {
      error: "xyz"
      error_description: "desc"
      error_uri: "uri"
    }
    expect(AccessTokenError.parse(path)).toEqual(expectedResult)
  )

  it("should ignore other parts in URL fragment when parsing access token error", () ->
    path = "error=xyz&error_description=desc&error_uri=uri&xyz=123"

    expectedResult = {
      error: "xyz"
      error_description: "desc"
      error_uri: "uri"
    }
    expect(AccessTokenError.parse(path)).toEqual(expectedResult)
  )

  

)