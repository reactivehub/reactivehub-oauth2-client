describe("AuthorizationRequest", () ->

  beforeEach(() ->
    module("taucentric.client.oauth2")
  )

  it("should generate correct request query string (all params)", () ->
    atr = new AuthorizationRequest("respType1", "clientID1", "redirectURI1", "scope1", "state1")
    expect(atr.getQueryParams()).toEqual("response_type=respType1&client_id=clientID1&redirect_uri=redirectURI1&scope=scope1&state=state1")
  )

  it("should generate correct request query string (params subset)", () ->
    atr = new AuthorizationRequest("respType1", "clientID1", null, null, "state1")
    expect(atr.getQueryParams()).toEqual("response_type=respType1&client_id=clientID1&state=state1")
  )

  it("should validate access token request (valid)", () ->
    atr = new AuthorizationRequest("respType1", "clientID1", "redirectURI1", "scope1", "state1")

    promise = atr.validate()

    assertAsync(() ->
      promise.isFulfilled()
    ,() ->
    )
  )

  it("should validate access token request (invalid)", () ->
    atr = new AuthorizationRequest("respType1", null, "redirectURI1", "scope1", "state1")

    promise = atr.validate()

    assertAsync(() ->
      promise.isRejected()
    ,() ->
    )
  )

)