describe("AccessToken", () ->

  beforeEach(() ->
    module("taucentric.client.oauth2")
  )

  it("should parse access token response from the URL fragment", () ->
    path = "access_token=xyz&token_type=type1&expires_in=1000&scope=scopex&state=abcde"

    expectedResult = {
      access_token: "xyz"
      token_type: "type1"
      expires_in: "1000"
      scope: "scopex"
      state: "abcde"
    }
    expect(AccessTokenResponse.parse(path)).toEqual(expectedResult)
  )

  it("should ignore other parts in URL fragment when parsing access token response", () ->
    path = "access_token=xyz&token_type=type1&foo=bar&xyz=123"

    expectedResult = {
      access_token: "xyz"
      token_type: "type1"
    }
    expect(AccessTokenResponse.parse(path)).toEqual(expectedResult)
  )

  

)