describe("AuthService", () ->

  service = null

  beforeEach(() ->
    module("taucentric.client.oauth2")
    inject((authService) ->
      service = authService
    )
  )

  it("should provide token if token initialization already complete", () ->
    service.tokenInitializationComplete = true
    service.tokenInfo = {
      token: "Foo"
    }

    tokenPromise = service.getToken()

    assertAsync(() ->
      tokenPromise.isFulfilled()
    ,() ->
      value = tokenPromise.inspect().value
      expect(value).toMatch("Foo")
    )
  )

  it("should reject token promise if token initialization complete but token not found", () ->
    service.tokenInitializationComplete = true
    service.tokenInfo = {}

    tokenPromise = service.getToken()

    assertAsync(() ->
      tokenPromise.isRejected()
    ,() ->
      value = tokenPromise.inspect().reason
      expect(value).toMatch("not_found")
    )
  )

  it("should resolve token promise once token initialization is complete", () ->
    service.tokenInitializationComplete = false
    tokenPromise = service.getToken()
    tokenPromise2 = service.getToken()

    service.tokenInfo = {}
    service.tokenInfo = {
      token: "Foo"
    }
    service.resolveTokenRequests()

    assertAsync(() ->
      tokenPromise.isFulfilled()
    ,() ->
      value = tokenPromise.inspect().value
      expect(value).toMatch("Foo")
    )

    assertAsync(() ->
      tokenPromise2.isFulfilled()
    ,() ->
      value = tokenPromise2.inspect().value
      expect(value).toMatch("Foo")
    )
  )

  it("should validate expiration time (failed)", () ->

    service.tokenInitializationComplete = true
    service.getNowPlus = () -> 10
    service.tokenInfo = {
      token: "xyz"
      expirationTime: 10
    }

    tokenPromise = service.getToken()

    assertAsync(() ->
      tokenPromise.isRejected()
    ,() ->
      value = tokenPromise.inspect().reason
      expect(value).toMatch("expired")
    )

  )

  it("should validate expiration time (success)", () ->
    service.tokenInitializationComplete = true
    service.getNowPlus = () -> 10
    service.tokenInfo = {
      token: "xyz"
      expirationTime: 11
    }

    tokenPromise = service.getToken()

    assertAsync(() ->
      tokenPromise.isFulfilled()
    ,() ->
      value = tokenPromise.inspect().value
      expect(value).toMatch(service.tokenInfo.token)
    )
  )

)