describe("AccessTokenErrorExtractor", () ->

  injector = null

  beforeEach(() ->
    module("taucentric.client.oauth2")
    inject(($injector) ->
      injector = $injector
    )
  )


  it("should parse error object and reject with it if present (error)", () ->
    path = "/error=foo"

    atre = new AccessTokenErrorExtractor(path, injector)
    promise = atre.extract("prev")

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      result = promise.inspect().reason
      expect(result).toEqual({error:"foo"})
    )
  )

  it("should parse error object and reject with it if present (error, desc)", () ->
    path = "/error=foo&error_description=bar"

    atre = new AccessTokenErrorExtractor(path, injector)
    promise = atre.extract("prev")

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      result = promise.inspect().reason
      expect(result).toEqual({error:"foo", error_description:"bar"})
    )
  )

  it("should parse error object and reject with it if present (error, desc, uri)", () ->
    path = "/error=foo&error_description=bar&error_uri=bax"

    atre = new AccessTokenErrorExtractor(path, injector)
    promise = atre.extract("prev")

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      result = promise.inspect().reason
      expect(result).toEqual({error:"foo", error_description:"bar", error_uri:"bax"})
    )
  )

  it("should reject with prev rejection reason if error not found in path", () ->
    path = "/err=x"

    atre = new AccessTokenErrorExtractor(path, injector)
    promise = atre.extract("prev")

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      result = promise.inspect().reason
      expect(result).toEqual("prev")
    )
  )

)