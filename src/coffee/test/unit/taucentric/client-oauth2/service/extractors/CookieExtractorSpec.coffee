describe("CookieExtractor", () ->

  injector = null

  beforeEach(() ->
    module("taucentric.client.oauth2")
    inject(($injector) ->
      injector = $injector
    )
  )

  it("should extract token info from cookie (passed)", () ->
    path = ""

    ce = new CookieExtractor(path, injector)
    ce.validateExpirationTime = () -> true
    ce.getTokenStorageService = () -> {
      getTokenInfo: () -> {
        token: "xyz"
        expirationTime: 2000
      }
    }
    
    promise = ce.extract()

    assertAsync(() ->
      promise.isFulfilled()
    ,() ->
      result = promise.inspect().value
      expect(result.token).toEqual("xyz")
      expect(result.expirationTime).toEqual(2000)
    )
  )

  it("should extract token info from cookie (missing cookie)", () ->
    path = ""

    ce = new CookieExtractor(path, injector)
    ce.validateExpirationTime = () -> true
    ce.getTokenStorageService = () -> {
      getTokenInfo: () -> null
    }
    
    promise = ce.extract()

    assertAsync(() ->
      promise.isRejected()
    ,() ->
    )
  )

  xit("should extract token info from cookie (expiration time validation failed)", () ->
    path = ""

    ce = new CookieExtractor(path, injector)
    ce.validateExpirationTime = () -> false
    ce.getTokenStorageService = () -> {
      getTokenInfo: () -> {
        token: "xyz"
        expirationTime: 2000
      }
    }
    
    promise = ce.extract()

    assertAsync(() ->
      promise.isRejected()
    ,() ->
    )
  )

)