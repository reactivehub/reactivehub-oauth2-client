describe("AccessTokenResponseExtractor", () ->

  injector = null

  beforeEach(() ->
    module("taucentric.client.oauth2")
    inject(($injector) ->
      injector = $injector
    )
  )

  it("should validate response state according to state stored in cookies (valid)", () ->
    path = "access_token=xyz&token_type=type1&state=abcd"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.getTokenStorageService = () -> {
      getState: () -> "abcd"
    }

    accessTokenResponse = {
      state: "abcd"
    }
    expect(atre.hasValidResponseState(accessTokenResponse)).toBe(true)
  )

  it("should validate response state according to state stored in cookies (not valid)", () ->
    path = "access_token=xyz&token_type=type1&state=abcd"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.getTokenStorageService = () -> {
      getState: () -> "xyz"
    }

    accessTokenResponse = {
      state: "abcd"
    }
    expect(atre.hasValidResponseState(accessTokenResponse)).toBeFalsy()
  )

  it("should validate response state according to state stored in cookies (state missing in access token response)", () ->
    path = "access_token=xyz&token_type=type1&state=abcd"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.getTokenStorageService = () -> {
      getState: () -> "xyz"
    }

    accessTokenResponse = {}
    expect(atre.hasValidResponseState(accessTokenResponse)).toBeFalsy()
  )

  it("should validate response state according to state stored in cookies (state missing in cookies)", () ->
    path = "access_token=xyz&token_type=type1&state=abcd"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.getTokenStorageService = () -> {
      getState: () -> null
    }

    accessTokenResponse = {
      state: "xyz"
    }
    expect(atre.hasValidResponseState(accessTokenResponse)).toBeFalsy()
  )

  it("should extract token info from access token response", () ->
    path = "access_token=xyz&token_type=type1&state=abcd"

    atre = new AccessTokenResponseExtractor(path, injector)

    atre.getExpirationTime = () -> 1000
    
    accessTokenResponse = {
      access_token: "xyz"
      token_type: "type1"
      expires_in: "1000"
      scope: "scopex"
      state: "abcde"
    }

    expectedResult = {
      token: "xyz"
      expirationTime: 1000
    }
    expect(atre.extractTokenInfoFromResponse(accessTokenResponse)).toEqual(expectedResult)
  )

  it("should validate access token response (valid)", () ->
    path = "access_token=xyz&token_type=type1&state=abcd"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.hasValidResponseState = () -> true
    
    accessTokenResponse = {
      access_token: "xyz"
      token_type: "type1"
      expires_in: "1000"
      scope: "scopex"
      state: "abcde"
    }

    expect(atre.validateAccessTokenResponse(accessTokenResponse)).toBe(true)
  )

  it("should validate access token response (access_token missing)", () ->
    path = "access_token=xyz&token_type=type1&state=abcd"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.hasValidResponseState = () -> true
    
    accessTokenResponse = {}

    expect(atre.validateAccessTokenResponse(accessTokenResponse)).toBeFalsy()
  )

  it("should validate access token response (invalid state)", () ->
    path = "/access_token=xyz&token_type=type1&state=abcd"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.hasValidResponseState = () -> false

    promise = atre.extract()

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      result = promise.inspect().reason
      expect(result).toEqual("invalid_state")
    )
  )

  it("should convert expires_in into expirationTime", () ->
    path = "access_token=xyz&token_type=type1&state=abcd"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.getNow = () -> 5000000

    expect(atre.getExpirationTime(2000)).toEqual(7000000)
    expect(atre.getExpirationTime()).toEqual(8600000)
  )

  it("should extract token info from URL fragment (passed)", () ->
    path = "/access_token=xyz&token_type=type1&state=abcd&expires_in=500"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.hasValidResponseState = () -> true
    atre.getNow = () -> 1000000

    promise = atre.extract()

    assertAsync(() ->
      promise.isFulfilled()
    ,() ->
      result = promise.inspect().value
      expect(result.token).toEqual("xyz")
      expect(result.expirationTime).toEqual(1500000)
    )
  )

  it("should extract token info from URL fragment (rejected for missing access_token)", () ->
    path = "/token_type=type1&state=abcd&expires_in=500"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.hasValidResponseState = () -> true
    atre.getNow = () -> 1000000

    promise = atre.extract()

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      
    )
  )

  it("should extract token info from URL fragment (rejected for missing state)", () ->
    path = "/access_token=1234&token_type=type1"

    atre = new AccessTokenResponseExtractor(path, injector)
    atre.getNow = () -> 1000000

    promise = atre.extract()

    assertAsync(() ->
      promise.isRejected()
    ,() ->
      
    )
  )

)