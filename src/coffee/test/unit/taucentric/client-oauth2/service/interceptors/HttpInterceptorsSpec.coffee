describe("HttpInterceptors", () ->

  authService = null
  revokeTokenSpy = null
  q = null

  beforeEach(() ->
    module("taucentric.client.oauth2")

    inject(($q) ->
      q = $q
    )

    authService = {
      config: {
        apiURLMatcher: "http://api.foo.com"
        loginRedirectionFilter: (status) -> status == 401
      }

      getToken: () -> Q.resolve("xyz")
      revokeTokenInfo: () ->
    }

    revokeTokenSpy = spyOn(authService, "revokeTokenInfo")
  )

  describe("HttpRequestInterceptor", () ->
    it("should include Authorization header if requested URL starts with api URL prefix", () ->
      config = {
        url: "http://api.foo.com/resource"
      }
      modifiedConfig = HttpRequestInterceptor.intercept(config, authService)

      assertAsync(() ->
        modifiedConfig.isFulfilled()
      ,() ->
        value = modifiedConfig.inspect().value
        expect(value.headers.Authorization).toMatch("Bearer xyz")
      )
    )

    it("should not include Authorization header if requested URL does not starts with api URL prefix", () ->
      config = {
        url: "http://api.bar.com"
      }
      modifiedConfig = HttpRequestInterceptor.intercept(config, authService)
      expect(modifiedConfig.headers).not.toBeDefined()
    )

    it("should include Authorization header if requested URL passed apiURLMatcher function", () ->
      config = {
        url: "http://api.foo.com/resource"
      }

      authService.config.apiURLMatcher = (str) -> true
      
      modifiedConfig = HttpRequestInterceptor.intercept(config, authService)

      assertAsync(() ->
        modifiedConfig.isFulfilled()
      ,() ->
        value = modifiedConfig.inspect().value
        expect(value.headers.Authorization).toMatch("Bearer xyz")
      )
    )

    it("should not include Authorization header if requested URL does not pass apiURLMatcher function", () ->
      config = {
        url: "http://api.bar.com"
      }
      
      authService.config.apiURLMatcher = (str) -> false

      modifiedConfig = HttpRequestInterceptor.intercept(config, authService)
      expect(modifiedConfig.headers).not.toBeDefined()
    )
  )

  describe("HttpResponseInterceptor", () ->
    it("should redirect to login page when error 401 is received", () ->
      rejection = {
        status: 401
        config: {
          url: "http://api.foo.com/resource"
        }
      }
      modifiedConfig = HttpResponseInterceptor.intercept(q, rejection, authService)
      expect(revokeTokenSpy).toHaveBeenCalled()
    )

    it("should not redirect to login page when status does not pass loginRedirectionFilter", () ->
      rejection = {
        status: 402
        config: {
          url: "http://api.foo.com/resource"
        }
      }
      modifiedConfig = HttpResponseInterceptor.intercept(q, rejection, authService)
      expect(revokeTokenSpy).not.toHaveBeenCalled()
    )

    it("should not redirect to login page if requested URL does not starts with api URL prefix", () ->
      rejection = {
        status: 401
        config: {
          url: "http://api.bar.com/resource"
        }
      }
      modifiedConfig = HttpResponseInterceptor.intercept(q, rejection, authService)
      expect(revokeTokenSpy).not.toHaveBeenCalled()
    )

    
  )

)