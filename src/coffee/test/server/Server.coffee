express = require("express")
uuid    = require("node-uuid")

app = express()



allowCrossDomain = (req, res, next) ->
  res.header("Access-Control-Allow-Origin", "http://localhost:8080")
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE")
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization")

  next()

app.configure(() ->
  app.use(allowCrossDomain)
  app.use(express.bodyParser())
)

app.get("/", (req, res) ->
  res.send("TauCentric Client OAuth2 Module Test Server running ...")
)

app.get("/approve", (req, res) ->
  console.log("Returning access_token to the client ...")
  state = req.query.state
  console.log(state)
  res.redirect("http://localhost:8080/#access_token=#{uuid.v1()}&state=#{state}&token_type=example&expires_in=3600")
)

app.get("/deny", (req, res) ->
  console.log("Returning access_token to the client ...")
  res.redirect("http://localhost:8080/#error=invalid_request&error_description=errorDescription&error_uri=uri")
)

app.get("/authorize", (req, res) ->
  res.sendfile(__dirname + "/authorize.html")
)

app.get("/resource", (req, res) ->
  console.log("Requested with token: #{req.headers['authorization']}")
  res.send("Resource representation")
)

app.get("/resourceInvalid", (req, res) ->
  console.log("Requested with token: #{req.headers['authorization']}")
  res.status(401)
  res.send("Error")
)



app.listen(3000)
console.log("Server running ...")