class AuthorizedController
  
  constructor: (@$scope, $http) ->

    @$scope.getResource = () ->
      $http.get("http://localhost:3000/resource").then((data) =>
        @resourceData = data.data
      ,(error) ->
        console.log(error)
      )

    @$scope.getInvalidResource = () ->
      $http.get("http://localhost:3000/resourceInvalid").then((data) =>
        @resourceData = data.data
      ,(error) ->
        console.log(error)
      )

@AuthorizedController = AuthorizedController

testApp = angular.module("taucentric.client.oauth2.test", ["ngRoute","taucentric.client.oauth2"])

testApp.config(["$routeProvider","authServiceProvider", ($routeProvider, authServiceProvider) ->

  authServiceProvider.config({
    clientID: "TaucentricAuthTest"
    redirectURI: "http://localhost:8080"
    authorizeEndpoint: "http://localhost:3000/authorize"
    apiURLMatcher: "http://localhost:3000"
    tokenInfoCookieName: "authTokenInfo"
  }).fail((error) ->
    console.log(error)
  )

  $routeProvider.when("/authorized",
    {
      templateUrl: "authorized.html"
      controller: "AuthorizedController"
    }
  ).when("/redirected",
    {
      templateUrl: "authorize.html"
    }
  ).otherwise({
    redirectTo: "/authorized"
  })

])



