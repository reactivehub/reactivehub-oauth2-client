module.exports = (grunt) =>

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  grunt.initConfig
  
    clean:
      build: "build"
      dist: "dist"

    coffee:
      module:
        options:
          sourceMap: true
          sourceRoot: ""
          manageDependencies: true
          bare: true
        files: 
          "build/taucentric-client-oauth2.js": ["src/coffee/module/**/*.coffee"]
      testui:
        files:
          "build/taucentric-client-oauth2-test.js" : ["src/coffee/test/ui/**/*.coffee"]

    coffeelint:
      module: ["src/coffee/module/**/*.coffee"]
      unit: ["src/coffee/test/unit/**/*.coffee"]
      testui: ["src/coffee/test/ui/**/*.coffee"]
      server: ["src/coffee/test/server/**/*.coffee"]
      options:
        max_line_length:
          level: "ignore"

    copy:
      package:
        files: [
          {
            cwd: "build",
            src: "**",
            dest: "dist/",
            expand: true
          }
        ]

    connect:
      options:
        port: 8080
        hostname: "localhost"
      server:
        options:
          middleware: (connect) ->
            [mount(connect, "build"), mount(connect, "src/html"), mount(connect, "components")]


    karma:
      options:
        configFile: "conf/karma.conf.coffee"
      unit:
        singleRun: false
        background: true
      continuous:
        singleRun: true
        background: false

    watch:
      
      module:
        files: "src/coffee/module/**/*.coffee"
        tasks: ["coffeelint:module", "coffee:module", "karma:unit:run"]
      testunit:
        files: "src/coffee/test/unit/**/*.coffee"
        tasks: ["coffeelint:unit", "karma:unit:run"]
      testui:
        files: "src/coffee/test/ui/**/*.coffee"
        tasks: ["coffeelint:testui", "coffee:testui"]
      server:
        files: "src/coffee/test/server/**/*.coffee"
        tasks: ["coffeelint:server", "express:test"]
        options:
          nospawn: true

    express:
      test:
        options:
          cmd: "coffee"
          script: "src/coffee/test/server/Server.coffee"

  grunt.registerTask "build", [
    "clean:build"
    "coffeelint"
    "coffee"
    "clean:dist"
    "copy:package"
  ]

  grunt.registerTask "test", ["karma:unit"]

  grunt.registerTask "default", ["build"]

  grunt.registerTask "develop", [
    "karma:unit"
    "express:test"
    "connect"
    "watch"
  ]

mount = (connect, dir) ->
  connect.static require("path").resolve(dir)