module.exports = (config) ->
    
    config.set({
        basePath: "../"
        frameworks: ["jasmine"]
        files: [
            "components/jquery/dist/jquery.js"
            "components/q/q.js"
            "components/angular/angular.js"
            "components/angular-route/angular-route.js"
            "components/angular-cookies/angular-cookies.js"
            "components/angular-mocks/angular-mocks.js"
            "components/underscore/underscore.js"
            "build/taucentric-client-oauth2.js"
            "src/coffee/test/unit/**/*.coffee"
        ]
        browsers: ["Chrome","PhantomJS"]
        reporters: ["dots"]
        captureTimeout: 5000
        singleRun: false
    })
