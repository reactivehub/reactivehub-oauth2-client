(function() {
  var AuthorizedController, testApp;

  AuthorizedController = (function() {
    function AuthorizedController($scope, $http) {
      this.$scope = $scope;
      this.$scope.getResource = function() {
        var _this = this;
        return $http.get("http://localhost:3000/resource").then(function(data) {
          return _this.resourceData = data.data;
        }, function(error) {
          return console.log(error);
        });
      };
      this.$scope.getInvalidResource = function() {
        var _this = this;
        return $http.get("http://localhost:3000/resourceInvalid").then(function(data) {
          return _this.resourceData = data.data;
        }, function(error) {
          return console.log(error);
        });
      };
    }

    return AuthorizedController;

  })();

  this.AuthorizedController = AuthorizedController;

  testApp = angular.module("taucentric.client.oauth2.test", ["ngRoute", "taucentric.client.oauth2"]);

  testApp.config([
    "$routeProvider", "authServiceProvider", function($routeProvider, authServiceProvider) {
      authServiceProvider.config({
        clientID: "TaucentricAuthTest",
        redirectURI: "http://localhost:8080",
        authorizeEndpoint: "http://localhost:3000/authorize",
        apiURLMatcher: "http://localhost:3000",
        tokenInfoCookieName: "authTokenInfo"
      }).fail(function(error) {
        return console.log(error);
      });
      return $routeProvider.when("/authorized", {
        templateUrl: "authorized.html",
        controller: "AuthorizedController"
      }).when("/redirected", {
        templateUrl: "authorize.html"
      }).otherwise({
        redirectTo: "/authorized"
      });
    }
  ]);

}).call(this);
