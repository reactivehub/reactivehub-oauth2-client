var AccessTokenError, AccessTokenErrorExtractor, AccessTokenResponse, AccessTokenResponseExtractor, ApiURLMatcher, AuthService, AuthServiceConfigValidator, AuthServiceProvider, AuthorizationRequest, AuthorizationRequestService, ConfigValidator, CookieExtractor, HttpRequestInterceptor, HttpResponseInterceptor, ModuleNames, QueryParser, RandomStringService, ScopeUtil, TokenExtractor, TokenStorageService, authModule, _ref, _ref1, _ref2, _ref3,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

ModuleNames = {
  ModuleName: "taucentric.client.oauth2",
  Services: {
    AuthService: "authService",
    TokenStorageService: "tokenStorageService",
    AuthorizationRequestService: "authorizationRequestService",
    RandomStringService: "randomStringService"
  },
  Constants: {
    Extractors: "authService.extractors"
  }
};

authModule = angular.module(ModuleNames.ModuleName, ["ngCookies"]);

ConfigValidator = (function() {
  function ConfigValidator(config, resources) {
    this.config = config;
    this.resources = resources;
  }

  ConfigValidator.prototype.runValidation = function() {};

  ConfigValidator.prototype.validate = function() {
    return this.runValidation();
  };

  ConfigValidator.prototype.validateRequiredParams = function(params) {
    var errorMessage, parameterName;
    for (parameterName in params) {
      errorMessage = params[parameterName];
      if (!this.config || !this.config[parameterName]) {
        return Q.reject(errorMessage);
      }
    }
    return Q(this.resources.Validation.Messages.AllRequiredParamsPresent);
  };

  return ConfigValidator;

})();

AuthServiceConfigValidator = (function(_super) {
  __extends(AuthServiceConfigValidator, _super);

  function AuthServiceConfigValidator() {
    _ref = AuthServiceConfigValidator.__super__.constructor.apply(this, arguments);
    return _ref;
  }

  AuthServiceConfigValidator.parameters = ["clientID", "redirectURI", "authorizeEndpoint", "apiURLMatcher", "loginRedirectionFilter", "tokenInfoCookieName", "stateCookieName", "sessionCookieNames"];

  AuthServiceConfigValidator.prototype.runValidation = function() {
    return this.validateRequiredParams({
      clientID: this.resources.Validation.Errors.ClientIDNotSpecified,
      redirectURI: this.resources.Validation.Errors.RedirectURINotSpecified,
      authorizeEndpoint: this.resources.Validation.Errors.AuthorizeEndpointNotSpecified,
      apiURLMatcher: this.resources.Validation.Errors.ApiURLMatcherNotSpecified
    });
  };

  return AuthServiceConfigValidator;

})(ConfigValidator);

authModule.directive("logoutButton", [
  "$compile", "$log", "authService", function($compile, $log, authService) {
    return {
      restrict: "A",
      link: function(scope, element, attrs) {
        scope.logoutAction = function() {
          return authService.logout();
        };
        return element.on("click", function(evt) {
          return ScopeUtil.safeApply(scope)(function() {
            return scope.logoutAction();
          });
        });
      }
    };
  }
]);

authModule.constant("authResources", {
  Validation: {
    Errors: {
      RedirectURINotSpecified: "Redirect URI not specified!",
      ClientIDNotSpecified: "Client ID not specified!",
      AuthorizeEndpointNotSpecified: "Login URL not specified!",
      ApiURLMatcherNotSpecified: "API URL matcher (prefix or function) not specified!"
    },
    Messages: {
      AllRequiredParamsPresent: "All required params present."
    }
  },
  Config: {
    Defaults: {
      TokenInfoCookieName: "apiAuthTokenInfo",
      StateCookieName: "apiAuthState",
      SessionCookieNames: ["session"],
      RevocationEndpoint: "/revoke"
    }
  }
});

TokenExtractor = (function() {
  function TokenExtractor(path, $injector) {
    this.path = path;
    this.$injector = $injector;
    this.log = this.getLog();
    this.q = this.$injector.get("$q");
  }

  TokenExtractor.prototype.extract = function() {
    return this.q.reject("Not implemented!");
  };

  TokenExtractor.prototype.getTokenStorageService = function() {
    return this.$injector.get("tokenStorageService");
  };

  TokenExtractor.prototype.getLog = function() {
    return this.$injector.get("$log");
  };

  return TokenExtractor;

})();

AccessTokenResponseExtractor = (function(_super) {
  __extends(AccessTokenResponseExtractor, _super);

  function AccessTokenResponseExtractor() {
    _ref1 = AccessTokenResponseExtractor.__super__.constructor.apply(this, arguments);
    return _ref1;
  }

  AccessTokenResponseExtractor.prototype.hasValidResponseState = function(accessTokenResponse) {
    return (accessTokenResponse != null ? accessTokenResponse.state : void 0) && accessTokenResponse.state === this.getTokenStorageService().getState();
  };

  AccessTokenResponseExtractor.prototype.extract = function(prevError) {
    var accessTokenResponse, path, result;
    result = null;
    path = this.path.substring(1);
    accessTokenResponse = this.parseAccessTokenResponse(path);
    if (!this.hasValidResponseState(accessTokenResponse)) {
      result = this.q.reject("invalid_state");
    } else if (!this.validateAccessTokenResponse(accessTokenResponse)) {
      result = this.q.reject("invalid_response");
    } else {
      result = this.q.when(this.extractTokenInfoFromResponse(accessTokenResponse));
    }
    this.getTokenStorageService().removeState();
    return result;
  };

  AccessTokenResponseExtractor.prototype.extractTokenInfoFromResponse = function(response) {
    return {
      token: response.access_token,
      expirationTime: this.getExpirationTime(response.expires_in)
    };
  };

  AccessTokenResponseExtractor.prototype.validateAccessTokenResponse = function(accessTokenResponse) {
    if (accessTokenResponse != null ? accessTokenResponse.access_token : void 0) {
      return true;
    } else {
      return false;
    }
  };

  AccessTokenResponseExtractor.prototype.getExpirationTime = function(expiresIn) {
    if (expiresIn) {
      return this.getNow() + expiresIn * 1000;
    } else {
      return null;
    }
  };

  AccessTokenResponseExtractor.prototype.getNow = function() {
    return Date.now();
  };

  AccessTokenResponseExtractor.prototype.parseAccessTokenResponse = function(path) {
    return AccessTokenResponse.parse(path);
  };

  return AccessTokenResponseExtractor;

})(TokenExtractor);

CookieExtractor = (function(_super) {
  __extends(CookieExtractor, _super);

  function CookieExtractor() {
    _ref2 = CookieExtractor.__super__.constructor.apply(this, arguments);
    return _ref2;
  }

  CookieExtractor.prototype.extract = function() {
    var tokenInfo, tokenStorage;
    tokenStorage = this.getTokenStorageService();
    tokenInfo = tokenStorage.getTokenInfo();
    if (tokenInfo) {
      return this.q.when(tokenInfo);
    } else {
      return this.q.reject("Token not found in token storage...");
    }
  };

  return CookieExtractor;

})(TokenExtractor);

AccessTokenErrorExtractor = (function(_super) {
  __extends(AccessTokenErrorExtractor, _super);

  function AccessTokenErrorExtractor() {
    _ref3 = AccessTokenErrorExtractor.__super__.constructor.apply(this, arguments);
    return _ref3;
  }

  AccessTokenErrorExtractor.prototype.extract = function(prevError) {
    var accessTokenError, path, result;
    result = null;
    path = this.path.substring(1);
    accessTokenError = this.parseAccessTokenError(path);
    if (accessTokenError.error) {
      return this.q.reject(accessTokenError);
    } else {
      return this.q.reject(prevError);
    }
  };

  AccessTokenErrorExtractor.prototype.parseAccessTokenError = function(path) {
    return AccessTokenError.parse(path);
  };

  return AccessTokenErrorExtractor;

})(TokenExtractor);

AuthService = (function() {
  function AuthService(config, $injector) {
    var _this = this;
    this.config = config;
    this.$injector = $injector;
    this.extractWithExtractor = __bind(this.extractWithExtractor, this);
    this.tokenRequests = [];
    this.tokenInitializationComplete = false;
    this.log = this.getLogService();
    this.originalPath = this.getLocationService().path();
    this.q = this.$injector.get("$q");
    this.initializeTokenInfo().then(function(tokenInfo) {
      _this.tokenInitializationComplete = true;
      _this.storeTokenInfo(tokenInfo);
      return _this.resolveTokenRequests();
    }, function(error) {
      _this.tokenInitializationComplete = true;
      _this.revokeTokenInfo();
      return _this.rejectTokenRequests(error);
    });
  }

  AuthService.prototype.getLocationService = function() {
    return this.$injector.get("$location");
  };

  AuthService.prototype.getLogService = function() {
    return this.$injector.get("$log");
  };

  AuthService.prototype.getTokenStorageService = function() {
    return this.$injector.get(ModuleNames.Services.TokenStorageService);
  };

  AuthService.prototype.getAuthorizationRequestService = function() {
    return this.$injector.get(ModuleNames.Services.AuthorizationRequestService);
  };

  AuthService.prototype.getExtractorClasses = function() {
    return this.$injector.get(ModuleNames.Constants.Extractors);
  };

  AuthService.prototype.getHttpService = function() {
    return this.$injector.get("$http");
  };

  AuthService.prototype.initializeTokenInfo = function() {
    var reduceHandler,
      _this = this;
    reduceHandler = function(extractorClass) {
      return function(prevError) {
        return _this.extractWithExtractor(extractorClass, prevError);
      };
    };
    return _.reduce(this.getExtractorClasses(), function(prev, extractorClass) {
      return prev.then(null, reduceHandler(extractorClass));
    }, this.q.reject("not_found"));
  };

  AuthService.prototype.extractWithExtractor = function(extractorClass, prevError) {
    var extractor;
    extractor = new extractorClass(this.originalPath, this.$injector);
    return extractor.extract(prevError);
  };

  AuthService.prototype.getToken = function() {
    var promise, result,
      _this = this;
    result = this.q.defer();
    if (this.tokenInitializationComplete) {
      this.resolveFutureWithToken(result);
    } else {
      this.addToTokenRequests(result);
    }
    promise = result.promise;
    promise.then(function(token) {
      return token;
    }, function(rejection) {
      return _this.goToAuthorizationServer();
    });
    return promise;
  };

  AuthService.prototype.validateExpirationTime = function(tokenInfo) {
    if (tokenInfo.expirationTime) {
      return tokenInfo.expirationTime > this.getNowPlus(10);
    } else {
      return true;
    }
  };

  AuthService.prototype.getNowPlus = function(minutes) {
    if (minutes == null) {
      minutes = 0;
    }
    return Date.now() + minutes * 60 * 1000;
  };

  AuthService.prototype.addToTokenRequests = function(future) {
    return this.tokenRequests.push(future);
  };

  AuthService.prototype.resolveTokenRequests = function() {
    var tokenRequest, _i, _len, _ref4;
    _ref4 = this.tokenRequests;
    for (_i = 0, _len = _ref4.length; _i < _len; _i++) {
      tokenRequest = _ref4[_i];
      this.resolveFutureWithToken(tokenRequest);
    }
    return this.tokenRequests = [];
  };

  AuthService.prototype.rejectTokenRequests = function(error) {
    var tokenRequest, _i, _len, _ref4;
    _ref4 = this.tokenRequests;
    for (_i = 0, _len = _ref4.length; _i < _len; _i++) {
      tokenRequest = _ref4[_i];
      this.rejectFutureWithError(tokenRequest, error);
    }
    return this.tokenRequests = [];
  };

  AuthService.prototype.resolveFutureWithToken = function(future) {
    if (this.tokenInfo && this.tokenInfo.token) {
      if (this.validateExpirationTime(this.tokenInfo)) {
        return future.resolve(this.tokenInfo.token);
      } else {
        return future.reject("expired");
      }
    } else {
      return future.reject("not_found");
    }
  };

  AuthService.prototype.rejectFutureWithError = function(future, error) {
    return future.reject(error);
  };

  AuthService.prototype.storeTokenInfo = function(tokenInfo) {
    this.tokenInfo = tokenInfo;
    return this.getTokenStorageService().storeTokenInfo(this.tokenInfo);
  };

  AuthService.prototype.revokeTokenInfo = function() {
    this.tokenInfo = null;
    return this.getTokenStorageService().revokeTokenInfo();
  };

  AuthService.prototype.generateAuthorizationRequest = function() {
    return this.getAuthorizationRequestService().generateRequest(this.config.clientID, this.config.redirectURI, this.config.scope);
  };

  AuthService.prototype.goToAuthorizationServer = function() {
    var authorizationRequest,
      _this = this;
    authorizationRequest = this.generateAuthorizationRequest();
    return authorizationRequest.validate().then(function() {
      var authorizeEndpointURL;
      authorizeEndpointURL = "" + _this.config.authorizeEndpoint + "?" + (authorizationRequest.getQueryParams());
      return window.location.replace(authorizeEndpointURL);
    }, function(error) {
      _this.log.error(error);
      return error;
    });
  };

  AuthService.prototype.logout = function() {
    var _this = this;
    return this.getHttpService().post(this.config.revocationEndpoint).then(function(result) {
      _this.revokeTokenInfo();
      return _this.goToAuthorizationServer();
    }, function(error) {
      return _this.log.error(error);
    });
  };

  AuthService.prototype.enrichURIWithAccessToken = function(uri) {
    var _this = this;
    uri = uri ? uri : "";
    return this.getToken().then(function(token) {
      return "" + uri + "?access_token=" + token;
    });
  };

  return AuthService;

})();

AuthServiceProvider = (function() {
  function AuthServiceProvider(authResources) {
    this.authResources = authResources;
  }

  AuthServiceProvider.prototype.getDefaultConfigData = function() {
    return {
      loginRedirectionFilter: function(code) {
        return code === 401;
      },
      tokenInfoCookieName: this.authResources.Config.Defaults.TokenInfoCookieName,
      stateCookieName: this.authResources.Config.Defaults.StateCookieName,
      sessionCookieNames: this.authResources.Config.Defaults.SessionCookieNames,
      revocationEndpoint: this.authResources.Config.Defaults.RevocationEndpoint
    };
  };

  AuthServiceProvider.prototype.extendDefaultConfig = function(configData) {
    var picked;
    picked = _.pick(configData, AuthServiceConfigValidator.parameters);
    return _.extend(this.getDefaultConfigData(), picked);
  };

  AuthServiceProvider.prototype.config = function(configData) {
    this.configData = this.extendDefaultConfig(configData);
    return this.validate(this.configData);
  };

  AuthServiceProvider.prototype.validate = function(config) {
    return new AuthServiceConfigValidator(config, this.authResources).validate();
  };

  AuthServiceProvider.prototype.$get = function($injector) {
    return new AuthService(this.configData, $injector);
  };

  return AuthServiceProvider;

})();

authModule.constant(ModuleNames.Constants.Extractors, [AccessTokenResponseExtractor, CookieExtractor, AccessTokenErrorExtractor]);

authModule.provider(ModuleNames.Services.AuthService, AuthServiceProvider);

authModule.run([ModuleNames.Services.AuthService, function(authService) {}]);

AuthorizationRequestService = (function() {
  AuthorizationRequestService.defaultTokenType = "token";

  function AuthorizationRequestService(tokenStorageService, randomStringService, log, q) {
    this.tokenStorageService = tokenStorageService;
    this.randomStringService = randomStringService;
    this.log = log;
    this.q = q;
  }

  AuthorizationRequestService.prototype.generateState = function() {
    return this.randomStringService.generate();
  };

  AuthorizationRequestService.prototype.generateAndStoreState = function() {
    var state;
    state = this.generateState();
    this.tokenStorageService.storeState(state);
    return state;
  };

  AuthorizationRequestService.prototype.generateRequest = function(clientID, redirectURI, scope) {
    var state;
    if (scope == null) {
      scope = null;
    }
    state = this.generateAndStoreState();
    return new AuthorizationRequest(AuthorizationRequestService.defaultTokenType, clientID, redirectURI, scope, state, this.q);
  };

  return AuthorizationRequestService;

})();

authModule.factory(ModuleNames.Services.AuthorizationRequestService, [
  ModuleNames.Services.TokenStorageService, ModuleNames.Services.RandomStringService, "$log", "$q", function(tokenStorageService, randomStringService, $log, $q) {
    return new AuthorizationRequestService(tokenStorageService, randomStringService, $log, $q);
  }
]);

TokenStorageService = (function() {
  function TokenStorageService(authService, $cookieStore, $rootScope, $log) {
    this.authService = authService;
    this.$cookieStore = $cookieStore;
    this.$rootScope = $rootScope;
    this.$log = $log;
  }

  TokenStorageService.prototype.getConfigValue = function(parameterName) {
    var _ref4;
    return (_ref4 = this.authService.config) != null ? _ref4[parameterName] : void 0;
  };

  TokenStorageService.prototype.getTokenCookieName = function() {
    return this.getConfigValue("tokenInfoCookieName");
  };

  TokenStorageService.prototype.getStateCookieName = function() {
    return this.getConfigValue("stateCookieName");
  };

  TokenStorageService.prototype.getSessionCookieNames = function() {
    return this.getConfigValue("sessionCookieNames");
  };

  TokenStorageService.prototype.getTokenInfo = function() {
    return this.get(this.getTokenCookieName());
  };

  TokenStorageService.prototype.storeTokenInfo = function(tokenInfo) {
    return this.store(this.getTokenCookieName(), tokenInfo);
  };

  TokenStorageService.prototype.revokeTokenInfo = function() {
    var sessionCookie, _i, _len, _ref4, _results;
    this.remove(this.getTokenCookieName());
    _ref4 = this.getSessionCookieNames() || [];
    _results = [];
    for (_i = 0, _len = _ref4.length; _i < _len; _i++) {
      sessionCookie = _ref4[_i];
      _results.push(this.remove(sessionCookie));
    }
    return _results;
  };

  TokenStorageService.prototype.getState = function() {
    return this.get(this.getStateCookieName());
  };

  TokenStorageService.prototype.storeState = function(state) {
    return this.store(this.getStateCookieName(), state);
  };

  TokenStorageService.prototype.removeState = function() {
    return this.remove(this.getStateCookieName());
  };

  TokenStorageService.prototype.get = function(name) {
    return this.$cookieStore.get(name);
  };

  TokenStorageService.prototype.store = function(name, value) {
    var _this = this;
    return ScopeUtil.safeApply(this.$rootScope)(function() {
      return _this.$cookieStore.put(name, value);
    });
  };

  TokenStorageService.prototype.remove = function(name) {
    var _this = this;
    return ScopeUtil.safeApply(this.$rootScope)(function() {
      return _this.$cookieStore.remove(name);
    });
  };

  return TokenStorageService;

})();

authModule.factory(ModuleNames.Services.TokenStorageService, [
  ModuleNames.Services.AuthService, "$cookieStore", "$rootScope", "$log", function(authService, $cookieStore, $rootScope, $log) {
    return new TokenStorageService(authService, $cookieStore, $rootScope, $log);
  }
]);

AccessTokenError = (function() {
  function AccessTokenError() {}

  AccessTokenError.errorParam = "error";

  AccessTokenError.errorDescriptionParam = "error_description";

  AccessTokenError.errorUriParam = "error_uri";

  AccessTokenError.params = [AccessTokenError.errorParam, AccessTokenError.errorDescriptionParam, AccessTokenError.errorUriParam];

  AccessTokenError.parse = function(path) {
    return QueryParser.parse(path, this.params);
  };

  return AccessTokenError;

})();

AccessTokenResponse = (function() {
  function AccessTokenResponse() {}

  AccessTokenResponse.accessTokenParam = "access_token";

  AccessTokenResponse.tokenTypeParam = "token_type";

  AccessTokenResponse.expiresInParam = "expires_in";

  AccessTokenResponse.scopeParam = "scope";

  AccessTokenResponse.stateParam = "state";

  AccessTokenResponse.params = [AccessTokenResponse.accessTokenParam, AccessTokenResponse.tokenTypeParam, AccessTokenResponse.expiresInParam, AccessTokenResponse.scopeParam, AccessTokenResponse.stateParam];

  AccessTokenResponse.parse = function(path) {
    return QueryParser.parse(path, this.params);
  };

  return AccessTokenResponse;

})();

AuthorizationRequest = (function() {
  AuthorizationRequest.responseTypeParam = "response_type";

  AuthorizationRequest.clientIDParam = "client_id";

  AuthorizationRequest.redirectURIParam = "redirect_uri";

  AuthorizationRequest.scopeParam = "scope";

  AuthorizationRequest.stateParam = "state";

  function AuthorizationRequest(responseType, clientID, redirectURI, scope, state, q) {
    this.responseType = responseType;
    this.clientID = clientID;
    this.redirectURI = redirectURI;
    this.scope = scope;
    this.state = state;
    this.q = q;
    this.params = [
      {
        key: AuthorizationRequest.responseTypeParam,
        value: this.responseType
      }, {
        key: AuthorizationRequest.clientIDParam,
        value: this.clientID
      }, {
        key: AuthorizationRequest.redirectURIParam,
        value: this.redirectURI
      }, {
        key: AuthorizationRequest.scopeParam,
        value: this.scope
      }, {
        key: AuthorizationRequest.stateParam,
        value: this.state
      }
    ];
  }

  AuthorizationRequest.prototype.validate = function() {
    var result;
    result = this.responseType && this.clientID && this.state;
    if (result) {
      return this.q.when(this);
    } else {
      return this.q.reject("Authorization Request does not contain all mandatory fields (response_type, client_id, state)!");
    }
  };

  AuthorizationRequest.prototype.getQueryParams = function() {
    var toKV;
    toKV = function(p) {
      return "" + p.key + "=" + (encodeURIComponent(p.value));
    };
    return _.reduce(this.params, function(prev, param) {
      var result;
      result = prev;
      if (param.value) {
        if (prev) {
          result = "" + prev + "&" + (toKV(param));
        } else {
          result = toKV(param);
        }
      }
      return result;
    }, "");
  };

  return AuthorizationRequest;

})();

HttpRequestInterceptor = (function() {
  function HttpRequestInterceptor() {}

  HttpRequestInterceptor.tokenType = "Bearer";

  HttpRequestInterceptor.intercept = function(config, authService) {
    var result, _ref4, _ref5;
    result = config;
    if (ApiURLMatcher.matches(config != null ? config.url : void 0, authService != null ? (_ref4 = authService.config) != null ? _ref4.apiURLMatcher : void 0 : void 0) || ApiURLMatcher.matches(config != null ? config.url : void 0, authService != null ? (_ref5 = authService.config) != null ? _ref5.revocationEndpoint : void 0 : void 0)) {
      result = authService.getToken().then(function(token) {
        var newHeaders, oldHeaders;
        oldHeaders = config.headers || {};
        newHeaders = _.extend(oldHeaders, {
          Authorization: "" + HttpRequestInterceptor.tokenType + " " + token
        });
        return _.extend(config, {
          headers: newHeaders
        });
      });
    }
    return result;
  };

  return HttpRequestInterceptor;

})();

HttpResponseInterceptor = (function() {
  function HttpResponseInterceptor() {}

  HttpResponseInterceptor.intercept = function($q, rejection, authService) {
    var _ref4, _ref5;
    if (ApiURLMatcher.matches((_ref4 = rejection.config) != null ? _ref4.url : void 0, authService != null ? (_ref5 = authService.config) != null ? _ref5.apiURLMatcher : void 0 : void 0) && authService.config.loginRedirectionFilter(rejection.status)) {
      authService.revokeTokenInfo();
    }
    return $q.reject(rejection);
  };

  return HttpResponseInterceptor;

})();

authModule.config([
  "$httpProvider", function($httpProvider) {
    return $httpProvider.interceptors.push(function($q, authService) {
      return {
        request: function(config) {
          return HttpRequestInterceptor.intercept(config, authService);
        },
        responseError: function(rejection) {
          return HttpResponseInterceptor.intercept($q, rejection, authService);
        }
      };
    });
  }
]);

ApiURLMatcher = (function() {
  function ApiURLMatcher() {}

  ApiURLMatcher.matches = function(url, apiURLMatcher) {
    if (_.isFunction(apiURLMatcher)) {
      return apiURLMatcher(url);
    } else {
      return url && url.startsWith(apiURLMatcher);
    }
  };

  return ApiURLMatcher;

})();

QueryParser = (function() {
  function QueryParser() {}

  QueryParser.parse = function(path, params) {
    return _.pick(_.reduce(_.map(path.split("&"), function(kv) {
      var kvList;
      return kvList = kv.split("=");
    }), function(res, tuple) {
      return _.extend(res, _.object([tuple[0]], [tuple[1]]));
    }, {}), params);
  };

  return QueryParser;

})();

RandomStringService = (function() {
  function RandomStringService() {}

  RandomStringService.prototype.generate = function() {
    if (this.hasCryptoSupport()) {
      return this.generateCryptoRandom();
    } else {
      return this.generateMathRandom();
    }
  };

  RandomStringService.prototype.hasCryptoSupport = function() {
    return _.isFunction(window.crypto.getRandomValues);
  };

  RandomStringService.prototype.generateCryptoRandom = function(size) {
    if (size == null) {
      size = 8;
    }
    return _.reduce(this.randomlyFillBuffer(8), function(prev, randomNumber) {
      return "" + prev + (randomNumber.toString(36));
    }, "");
  };

  RandomStringService.prototype.randomlyFillBuffer = function(size) {
    var buf;
    buf = new Uint8Array(size);
    window.crypto.getRandomValues(buf);
    return buf;
  };

  RandomStringService.prototype.generateMathRandom = function(size) {
    var sizePowered, _i, _ref4, _results,
      _this = this;
    if (size == null) {
      size = 8;
    }
    sizePowered = Math.pow(10, size);
    return _.reduce((function() {
      _results = [];
      for (var _i = 0, _ref4 = size - 1; 0 <= _ref4 ? _i <= _ref4 : _i >= _ref4; 0 <= _ref4 ? _i++ : _i--){ _results.push(_i); }
      return _results;
    }).apply(this), function(prev, num) {
      return "" + prev + (_this.generatePseudoRandomNumber().toString(36));
    }, "");
  };

  RandomStringService.prototype.generatePseudoRandomNumber = function(max) {
    if (max == null) {
      max = 36;
    }
    return Math.round(Math.random() * max);
  };

  return RandomStringService;

})();

authModule.factory(ModuleNames.Services.RandomStringService, [
  "$log", function($log) {
    return new RandomStringService($log);
  }
]);

ScopeUtil = (function() {
  function ScopeUtil() {}

  ScopeUtil.safeApply = function(scope) {
    return function(fn) {
      var phase;
      phase = scope.$root.$$phase;
      if (phase === "$apply" || phase === "$digest") {
        if (_.isFunction(fn)) {
          return fn();
        }
      } else {
        return scope.$apply(fn);
      }
    };
  };

  return ScopeUtil;

})();

if (!_.isFunction(String.prototype.startsWith)) {
  String.prototype.startsWith = function(str) {
    if (str) {
      return this.slice(0, str.length) === str;
    } else {
      return false;
    }
  };
}

/*
//@ sourceMappingURL=taucentric-client-oauth2.js.map
*/