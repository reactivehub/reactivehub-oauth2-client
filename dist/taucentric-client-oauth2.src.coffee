ModuleNames =
  ModuleName: "taucentric.client.oauth2"
  Services:
    AuthService: "authService"
    TokenStorageService: "tokenStorageService"
    AuthorizationRequestService: "authorizationRequestService"
    RandomStringService: "randomStringService"
  Constants:
    Extractors: "authService.extractors"

authModule = angular.module(ModuleNames.ModuleName, ["ngCookies"])

class ConfigValidator

  constructor: (@config, @resources) ->

  runValidation: () ->

  validate: () -> @runValidation()

  validateRequiredParams: (params) ->
    
    for parameterName, errorMessage of params
      if not @config or not @config[parameterName]
        return Q.reject(errorMessage)

    Q(@resources.Validation.Messages.AllRequiredParamsPresent)

#_require ConfigValidator

class AuthServiceConfigValidator extends ConfigValidator

  @parameters = [
    "clientID"
    "redirectURI"
    "authorizeEndpoint"
    "apiURLMatcher"
    "loginRedirectionFilter"
    "tokenInfoCookieName"
    "stateCookieName"
    "sessionCookieNames"
  ]

  runValidation: () ->
    @validateRequiredParams({
      clientID: @resources.Validation.Errors.ClientIDNotSpecified
      redirectURI: @resources.Validation.Errors.RedirectURINotSpecified
      authorizeEndpoint: @resources.Validation.Errors.AuthorizeEndpointNotSpecified
      apiURLMatcher: @resources.Validation.Errors.ApiURLMatcherNotSpecified
    })

authModule.directive("logoutButton", ["$compile", "$log", "authService", ($compile, $log, authService) ->
  {
    restrict: "A"

    link: (scope, element, attrs) ->

      scope.logoutAction = () ->
        authService.logout()

      element.on("click", (evt) ->
        ScopeUtil.safeApply(scope)(() ->
          scope.logoutAction()
        )
      )

  }
])

authModule.constant("authResources", {
  Validation:
    Errors:
      RedirectURINotSpecified: "Redirect URI not specified!"
      ClientIDNotSpecified: "Client ID not specified!"
      AuthorizeEndpointNotSpecified: "Login URL not specified!"
      ApiURLMatcherNotSpecified: "API URL matcher (prefix or function) not specified!"
    Messages:
      AllRequiredParamsPresent: "All required params present."
  Config:
    Defaults:
      TokenInfoCookieName: "apiAuthTokenInfo"
      StateCookieName: "apiAuthState"
      SessionCookieNames: ["session"]
      RevocationEndpoint: "/revoke"
})

class TokenExtractor
  
  constructor: (@path, @$injector) ->
    @log = @getLog()
    @q = @$injector.get("$q")

  extract: () -> @q.reject("Not implemented!")

  getTokenStorageService: () -> @$injector.get("tokenStorageService")

  getLog: () -> @$injector.get("$log")
#_require TokenExtractor

class AccessTokenResponseExtractor extends TokenExtractor
  
  hasValidResponseState: (accessTokenResponse) ->
    accessTokenResponse?.state and accessTokenResponse.state == @getTokenStorageService().getState()

  extract: (prevError) ->
    result = null
    path = @path.substring(1)
    accessTokenResponse = @parseAccessTokenResponse(path)

    if not @hasValidResponseState(accessTokenResponse)
      result = @q.reject("invalid_state")
    else if not @validateAccessTokenResponse(accessTokenResponse)
      result = @q.reject("invalid_response")
    else
      result = @q.when(@extractTokenInfoFromResponse(accessTokenResponse))

    @getTokenStorageService().removeState()

    result

  extractTokenInfoFromResponse: (response) ->
    {
      token: response.access_token
      expirationTime: @getExpirationTime(response.expires_in)
    }

  validateAccessTokenResponse: (accessTokenResponse) ->
    if accessTokenResponse?.access_token
      true
    else
      false

  getExpirationTime: (expiresIn) ->
    if expiresIn
      @getNow() + expiresIn * 1000
    else
      null

  getNow: () -> Date.now()

  parseAccessTokenResponse: (path) -> AccessTokenResponse.parse(path)


#_require TokenExtractor

class CookieExtractor extends TokenExtractor
  
  extract: () ->
    tokenStorage = @getTokenStorageService()

    tokenInfo = tokenStorage.getTokenInfo()
    if tokenInfo
      @q.when(tokenInfo)
    else
      @q.reject("Token not found in token storage...")

#_require TokenExtractor

class AccessTokenErrorExtractor extends TokenExtractor

  extract: (prevError) ->
    result = null
    path = @path.substring(1)
    accessTokenError = @parseAccessTokenError(path)

    if accessTokenError.error
      @q.reject(accessTokenError)
    else
      @q.reject(prevError)

  parseAccessTokenError: (path) -> AccessTokenError.parse(path)


#_require extractors/AccessTokenResponseExtractor
#_require extractors/CookieExtractor
#_require extractors/AccessTokenErrorExtractor

class AuthService

  constructor: (@config, @$injector) ->

    @tokenRequests = []
    @tokenInitializationComplete = false

    @log = @getLogService()
    @originalPath = @getLocationService().path()
    @q = @$injector.get("$q")

    @initializeTokenInfo().then((tokenInfo) =>
      @tokenInitializationComplete = true
      @storeTokenInfo(tokenInfo)
      @resolveTokenRequests()
    , (error) =>
      @tokenInitializationComplete = true
      @revokeTokenInfo()
      @rejectTokenRequests(error)
    )

  getLocationService: () ->             @$injector.get("$location")
  getLogService: () ->                  @$injector.get("$log")
  getTokenStorageService: () ->         @$injector.get(ModuleNames.Services.TokenStorageService)
  getAuthorizationRequestService: () -> @$injector.get(ModuleNames.Services.AuthorizationRequestService)
  getExtractorClasses: () ->            @$injector.get(ModuleNames.Constants.Extractors)
  getHttpService: () ->                 @$injector.get("$http")

  # Method tries to extract token from different places by executing
  # list of specified TokenExtractors taking the first result returning
  # token value.
  #
  # AccessTokenResponseExtractor - tries to extract token from URL fragment.
  # URL fragment contains token when redirected back from authorization
  # server.
  #
  # CookieExtractor - tries to retrieve the token from cookie

  initializeTokenInfo: () ->

    reduceHandler = (extractorClass) => (prevError) => @extractWithExtractor(extractorClass, prevError)

    _.reduce(@getExtractorClasses(),(prev, extractorClass) =>
      prev.then(null, reduceHandler(extractorClass))
    , @q.reject("not_found"))


  extractWithExtractor: (extractorClass, prevError) =>
    extractor = new extractorClass(@originalPath, @$injector)
    extractor.extract(prevError)

  getToken: () ->
    result = @q.defer()
    if @tokenInitializationComplete
      @resolveFutureWithToken(result)
    else
      @addToTokenRequests(result)

    promise = result.promise

    promise.then((token) ->
      token
    , (rejection) =>
      @goToAuthorizationServer()
    )

    promise


  validateExpirationTime: (tokenInfo) ->
    if tokenInfo.expirationTime
      tokenInfo.expirationTime > @getNowPlus(10)
    else
      true

  getNowPlus: (minutes = 0) -> Date.now() + minutes * 60 * 1000

  addToTokenRequests: (future) ->
    @tokenRequests.push(future)


  # Resolves awaiting token requests with token value.
  # If token not found, token requests are rejected with
  # 'not_found' error. If token is expired, token requests
  # are rejected with 'expired' error.
  resolveTokenRequests: () ->
    for tokenRequest in @tokenRequests
      @resolveFutureWithToken(tokenRequest)

    @tokenRequests = []

  rejectTokenRequests: (error) ->
    for tokenRequest in @tokenRequests
      @rejectFutureWithError(tokenRequest, error)

    @tokenRequests = []


  resolveFutureWithToken: (future) ->
    if @tokenInfo and @tokenInfo.token
      if @validateExpirationTime(@tokenInfo)
        future.resolve(@tokenInfo.token)
      else
        future.reject("expired")
    else
      future.reject("not_found")

  rejectFutureWithError: (future, error) ->
    future.reject(error)


  storeTokenInfo: (@tokenInfo) ->
    @getTokenStorageService().storeTokenInfo(@tokenInfo)

  # Method clears currently used token:
  # - clears locally kept token
  # - clears token stored in cookie
  # - redirects back to login page
  revokeTokenInfo: () ->
    @tokenInfo = null
    @getTokenStorageService().revokeTokenInfo()

  # Asks AuthorizationRequestService for fresh authorization request object.
  generateAuthorizationRequest: () ->
    @getAuthorizationRequestService().generateRequest(@config.clientID, @config.redirectURI, @config.scope)

  # Method redirects the browser to authorization endpoint
  # - it firstly generates authorization request
  # - then the request is validated (whether it contains all required params)
  # - if valid, browser is redirected to the authorization endpoint (else error is logged)
  goToAuthorizationServer: () ->
    authorizationRequest = @generateAuthorizationRequest()

    authorizationRequest.validate().then(() =>
      authorizeEndpointURL = "#{@config.authorizeEndpoint}?#{authorizationRequest.getQueryParams()}"

      #@log.log("Authorize Endpoint: #{authorizeEndpointURL}")
      window.location.replace(authorizeEndpointURL)
    ,(error) =>
      @log.error(error)
      error
    )

  logout: () ->
    @getHttpService().post(@config.revocationEndpoint).then((result) =>
      @revokeTokenInfo()
      @goToAuthorizationServer()
    , (error) =>
      @log.error(error)
    )



  enrichURIWithAccessToken: (uri) ->
    uri = if uri then uri else ""
    @getToken().then((token) =>
      "#{uri}?access_token=#{token}"
    )



# AuthServiceProvider is AuthService factory and is used
# to configure AuthService service.
#
# To configure AuthService, inject authServiceProvider in AngularJS
# config block and provide the configuration object by calling
# authServiceProvider.config(configObj) where configObj contains:
#
# clientID - (mandatory) ID of the client registered with Authorization Server
#
# redirectURI - (mandatory) redirect URI where the user-agent should be redirected when
# coming back from Authorization Server
#
# authorizeEndpoint - (mandatory) Authorization server endpoint where Access Token Request
# should be send.
#
# revocationEndpoint - (optional) Revocation endpoint where the request should be POSTed
# to revoke token. Default is /revoke
#
# apiURLMatcher - (mandatory) API URL matcher (either prefix string or string -> boolean function) -
# all calls to endpoints starting with specified api URL prefix (or provided function returns true for URL being called)
# will be intercepted and token will be included as Authorize HTTP request header.
#
# scope - (optional) Space-delimited list of required scopes
#
# loginRedirectionFilter - (optional) Function int -> boolean which determines
# whether HTTP response with specified error code should be redirected to
# login page.
#
# tokenInfoCookieName - (optional) Cookie name for storing token value. Default is apiAuthTokenInfo.
#
# stateCookieName - (optional) Cookie name for storing state being send to authorization server
# in access token request


class AuthServiceProvider

  constructor: (@authResources) ->

  getDefaultConfigData: () ->
    loginRedirectionFilter: (code) -> code == 401
    tokenInfoCookieName: @authResources.Config.Defaults.TokenInfoCookieName
    stateCookieName: @authResources.Config.Defaults.StateCookieName
    sessionCookieNames: @authResources.Config.Defaults.SessionCookieNames
    revocationEndpoint: @authResources.Config.Defaults.RevocationEndpoint

  extendDefaultConfig: (configData) ->
    picked = _.pick(configData, AuthServiceConfigValidator.parameters)
    _.extend(@getDefaultConfigData(), picked)

  config: (configData) ->
    @configData = @extendDefaultConfig(configData)
    @validate(@configData)

  validate: (config) ->
    new AuthServiceConfigValidator(config, @authResources).validate()

  $get: ($injector) ->
    new AuthService(@configData, $injector)


# AuthService extractor classes
authModule.constant(ModuleNames.Constants.Extractors, [
  AccessTokenResponseExtractor
  CookieExtractor
  AccessTokenErrorExtractor
])

# AuthServiceProvider registration
authModule.provider(ModuleNames.Services.AuthService, AuthServiceProvider)

# Explicitly inject authService to initialize the service.
authModule.run([ModuleNames.Services.AuthService, (authService) -> ])

class AuthorizationRequestService

  @defaultTokenType = "token"

  constructor: (@tokenStorageService, @randomStringService, @log, @q) ->

  # Generates random string to be used as state
  # parameter in access token request.
  generateState: () -> @randomStringService.generate()

  # Generates and stores state in token storage.
  # State will be matched with incoming state in query when browser
  # is redirected back from authorization server.
  generateAndStoreState: () ->
    state = @generateState()
    @tokenStorageService.storeState(state)
    state

  # Generates new authorization request object.
  #
  # - state is generated and stored as part of this method
  #
  generateRequest: (clientID, redirectURI, scope = null) ->
    state = @generateAndStoreState()
    new AuthorizationRequest(AuthorizationRequestService.defaultTokenType, clientID, redirectURI, scope, state, @q)
     
authModule.factory(ModuleNames.Services.AuthorizationRequestService, [
  ModuleNames.Services.TokenStorageService
  ModuleNames.Services.RandomStringService
  "$log"
  "$q"
  (tokenStorageService, randomStringService, $log, $q) ->
    new AuthorizationRequestService(tokenStorageService, randomStringService, $log, $q)
])
class TokenStorageService

  constructor: (@authService, @$cookieStore, @$rootScope, @$log) ->
    #@$log.log("TokenStorageService created ...")

  getConfigValue: (parameterName) -> @authService.config?[parameterName]
  getTokenCookieName: () -> @getConfigValue("tokenInfoCookieName")
  getStateCookieName: () -> @getConfigValue("stateCookieName")
  getSessionCookieNames: () -> @getConfigValue("sessionCookieNames")

  getTokenInfo: () -> @get(@getTokenCookieName())

  storeTokenInfo: (tokenInfo) ->
    @store(@getTokenCookieName(), tokenInfo)

  revokeTokenInfo: () ->
    @remove(@getTokenCookieName())
    for sessionCookie in (@getSessionCookieNames() or [])
      @remove(sessionCookie)

  getState: () -> @get(@getStateCookieName())

  storeState: (state) -> @store(@getStateCookieName(), state)

  removeState: () -> @remove(@getStateCookieName())


  get: (name) ->
    @$cookieStore.get(name)

  store: (name, value) ->
    ScopeUtil.safeApply(@$rootScope)(() =>
      @$cookieStore.put(name, value)
    )


  remove: (name) ->
    ScopeUtil.safeApply(@$rootScope)(() =>
      @$cookieStore.remove(name)
    )


authModule.factory(ModuleNames.Services.TokenStorageService, [ModuleNames.Services.AuthService, "$cookieStore", "$rootScope", "$log", (authService, $cookieStore, $rootScope, $log) ->
  new TokenStorageService(authService, $cookieStore, $rootScope, $log)
])

class AccessTokenError
  
  @errorParam = "error"
  @errorDescriptionParam = "error_description"
  @errorUriParam = "error_uri"

  @params = [
    @errorParam
    @errorDescriptionParam
    @errorUriParam
  ]

  @parse: (path) -> QueryParser.parse(path, @params)
    
class AccessTokenResponse
  
  @accessTokenParam = "access_token"
  @tokenTypeParam = "token_type"
  @expiresInParam = "expires_in"
  @scopeParam = "scope"
  @stateParam = "state"

  @params = [
    @accessTokenParam
    @tokenTypeParam
    @expiresInParam
    @scopeParam
    @stateParam
  ]

  @parse: (path) -> QueryParser.parse(path, @params)
class AuthorizationRequest

  @responseTypeParam = "response_type"
  @clientIDParam = "client_id"
  @redirectURIParam = "redirect_uri"
  @scopeParam = "scope"
  @stateParam = "state"
  
  constructor: (@responseType, @clientID, @redirectURI, @scope, @state, @q) ->
    @params = [
      {
        key: AuthorizationRequest.responseTypeParam
        value: @responseType
      }
      {
        key: AuthorizationRequest.clientIDParam
        value: @clientID
      }
      {
        key: AuthorizationRequest.redirectURIParam
        value: @redirectURI
      }
      {
        key: AuthorizationRequest.scopeParam
        value: @scope
      }
      {
        key: AuthorizationRequest.stateParam
        value: @state
      }
    ]

  validate: () ->
    result = @responseType and @clientID and @state
    if result
      @q.when(@)
    else
      @q.reject("Authorization Request does not contain all mandatory fields (response_type, client_id, state)!")

  getQueryParams: () ->

    toKV = (p) -> "#{p.key}=#{encodeURIComponent(p.value)}"

    _.reduce(@params, (prev, param) ->
      result = prev
      if param.value
        if prev
          result = "#{prev}&#{toKV(param)}"
        else
          result = toKV(param)
      result
    ,"")


class HttpRequestInterceptor

  @tokenType = "Bearer"

  # We need to include token in all requests to API or request to revocationEndpoint
  @intercept: (config, authService) ->
    result = config
    if ApiURLMatcher.matches(config?.url, authService?.config?.apiURLMatcher) or ApiURLMatcher.matches(config?.url, authService?.config?.revocationEndpoint)

      result = authService.getToken().then((token) ->
        oldHeaders = config.headers or {}
        newHeaders = _.extend(oldHeaders, {
          Authorization: "#{HttpRequestInterceptor.tokenType} #{token}"
        })
        _.extend(config, {headers: newHeaders})
      )

    result

class HttpResponseInterceptor

  @intercept: ($q, rejection, authService) ->
    if ApiURLMatcher.matches(rejection.config?.url, authService?.config?.apiURLMatcher) and authService.config.loginRedirectionFilter(rejection.status)
      authService.revokeTokenInfo()
    $q.reject(rejection)


authModule.config(["$httpProvider", ($httpProvider) ->
  $httpProvider.interceptors.push(($q, authService) ->
    request: (config) -> HttpRequestInterceptor.intercept(config, authService)
    responseError: (rejection) -> HttpResponseInterceptor.intercept($q, rejection, authService)
  )
])

class ApiURLMatcher

  @matches: (url, apiURLMatcher) ->
    if _.isFunction(apiURLMatcher)
      apiURLMatcher(url)
    else
      url and url.startsWith(apiURLMatcher)
class QueryParser
  
  @parse: (path, params) ->
    _.pick(_.reduce(_.map(path.split("&"),(kv) ->
      kvList = kv.split("=")
    ),(res, tuple) ->
      _.extend(res, _.object([tuple[0]], [tuple[1]]))
    ,{}), params)
class RandomStringService

  generate: () ->
    if @hasCryptoSupport()
      @generateCryptoRandom()
    else
      @generateMathRandom()

  hasCryptoSupport: () ->
    _.isFunction(window.crypto.getRandomValues)

  generateCryptoRandom: (size = 8) ->
    _.reduce(@randomlyFillBuffer(8),(prev, randomNumber) ->
      "#{prev}#{randomNumber.toString(36)}"
    ,"")

  randomlyFillBuffer: (size) ->
    buf = new Uint8Array(size)
    window.crypto.getRandomValues(buf)
    buf

  generateMathRandom: (size = 8) ->
    sizePowered = Math.pow(10, size)
    _.reduce([0..(size-1)],(prev, num) =>
      "#{prev}#{@generatePseudoRandomNumber().toString(36)}"
    ,"")

  generatePseudoRandomNumber: (max = 36) -> Math.round(Math.random() * max)


authModule.factory(ModuleNames.Services.RandomStringService, ["$log", ($log) ->
  new RandomStringService($log)
])
class ScopeUtil

  @safeApply: (scope) ->
    (fn) ->
      phase = scope.$root.$$phase

      if phase == "$apply" or phase == "$digest"
        if _.isFunction(fn)
          fn()
      else
        scope.$apply(fn)
if not _.isFunction(String::startsWith)
  String::startsWith = (str) ->
    if str
      @slice(0, str.length) == str
    else
      false